//****************************
// D A T A B A S E 
//****************************

// Initialise the database
function initDatabase () {
	var shortName = 'TIMEAPPDB';
	var version = '1.0';
	var displayName = 'Time_App_DBase';
	var maxSize = 100000;
	TIMEAPPDB = openDatabase (shortName, version, displayName, maxSize);
	createTables();
}

// Create database tables
function createTables () {
	TIMEAPPDB.transaction(
	function(transaction) {
		transaction.executeSql
			('CREATE TABLE IF NOT EXISTS matters (mattId INTEGER PRIMARY KEY, mattNo TEXT, opened TEXT, ufn TEXT, workType TEXT, clientForename TEXT, clientSurname TEXT, clientDob TEXT, matterType TEXT, charges TEXT, editDate TEXT );' , [], nullDataHandler, errorHandler);
		transaction.executeSql
			('CREATE TABLE IF NOT EXISTS timesheets (timeId INTEGER PRIMARY KEY, mattId INTEGER, timeDate TEXT, courtPolice TEXT, rate TEXT, duration TEXT, activity TEXT, dscc TEXT, comment TEXT, hearingType TEXT, repOrder TEXT, maatID TEXT, nextHearing TEXT, location TEXT, notes TEXT, editDate TEXT);' , [], nullDataHandler, errorHandler);
		transaction.executeSql
			('CREATE TABLE IF NOT EXISTS users (userId INTEGER PRIMARY KEY, surname TEXT, firstname TEXT, userCode TEXT, password TEXT, editDate TEXT );' , [], nullDataHandler, errorHandler);
		transaction.executeSql
			('CREATE TABLE IF NOT EXISTS settings (id INTEGER PRIMARY KEY, lastSync DATETIME, syncType TEXT, dbName TEXT, editDate TEXT );' , [], nullDataHandler, errorHandler);
		transaction.executeSql
			('CREATE TABLE IF NOT EXISTS rates (rateId INTEGER PRIMARY KEY, rate TEXT, description TEXT, amount REAL, editDate TEXT );' , [], nullDataHandler, errorHandler);
		transaction.executeSql
			('CREATE TABLE IF NOT EXISTS courtPolice (courtPoliceId INTEGER PRIMARY KEY, code TEXT, description TEXT, editDate TEXT );' , [], nullDataHandler, errorHandler);
	});
}

// Delete all tables from the database
function dropTables () {
	TIMEAPPDB.transaction(
	function(transaction) {
		transaction.executeSql('DROP TABLE matters;', [], nullDataHandler, errorHandler);
		transaction.executeSql('DROP TABLE timesheets;', [], nullDataHandler, errorHandler);
		transaction.executeSql('DROP TABLE users;', [], nullDataHandler, errorHandler);
		transaction.executeSql('DROP TABLE settings;', [], nullDataHandler, errorHandler);
		transaction.executeSql('DROP TABLE rates;', [], nullDataHandler, errorHandler);
		transaction.executeSql('DROP TABLE courtPolice;', [], nullDataHandler, errorHandler);	
		console.log ('Database Tables Dropped' ) ;
	});
}

function nullDataHandler () {
	console.log("SQL Query Succeded");
}
	
function errorHandler (transaction, error){
	console.log (' Error is:  '+error.message+' Code:  '+error.code+') ' ) ;
}

// Format date now -  yyyy-mm-dd
Date.prototype.dateNow = function () { 
    return this.getFullYear() +"-"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+ ((this.getDate() < 10)?"0":"") + this.getDate();
}

// Format time now - hh:mm:ss
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

// Set editDate
function setEditDate(){
	var editDate = new Date();
	return editDate.dateNow()+" "+editDate.timeNow();
}

// Load matters & timesheets data for testing
function loadTestData(){
	$.getJSON("./app/matt_test_data.json", function(mData){  
		$.each(mData, function(i, matt){
	    	console.log(matt);
	    	TIMEAPPDB.transaction( function(transaction) {
				transaction.executeSql ("INSERT INTO matters (mattNo, opened, ufn, workType, clientForename, clientSurname, clientDob, matterType, charges, "+
					"editDate) VALUES ('"+matt.mattNo+"', '"+matt.opened+"', '"+matt.ufn+"', '"+matt.workType+"', '"+matt.clientForename+"', '"+matt.clientSurname+"',"+
					"'"+matt.clientDob+"', '"+matt.matterType+"', '"+matt.charges+"', '"+setEditDate()+"')");
				console.log("Insert Matter Query Succeded");
			});

	   	});
 	});

	$.getJSON("./app/time_test_data.json", function(tData){  
		$.each(tData, function(i, time){
			console.log(time);
			
	    	TIMEAPPDB.transaction( function(transaction) {
				transaction.executeSql("INSERT INTO timesheets (mattId, timeDate, courtPolice, rate, duration, activity, dscc, comment, hearingType, "+
					"repOrder, maatID, nextHearing, location, notes, editDate) VALUES ('"+time.mattId+"', '"+time.timeDate+"', '"+time.courtPolice+"', "+
					"'"+time.rate+"', '"+time.duration+"', '"+time.activity+"', '"+time.dscc+"', '"+time.comment+"', '"+time.hearingType+"', "+
					"'"+time.repOrder+"', '"+time.maatID+"', '"+time.nextHearing+"', '"+time.location+"', '"+time.notes+"', '"+setEditDate()+"')");
				console.log("Insert Time Query Succeded");
			});
	   	});
 	});



}