//****************************
// T I M E S H E E T S
//****************************

// Insert time record
function insertTimeRecord(){
	TIMEAPPDB.transaction( function(transaction) {
		var data0 = $('#selectMatterAT').data('mattID');
		var data1 = $('#timeDate').val();
		var data2 = $('#courtPoliceID').val();
		var data3 = $('#timeRate').val();
		var data4 = $('#timeDurationQty').val();
		var data5 = $('#timeActivity').val();
		var data6 = $('#timeDSCC').val();		
		var data7 = $('#timeComment').val();
		var data8 = $('#timeHearingType').val();
		var data9 = $('#timeRepOrderDate').val();
		var data10 = $('#timeMaatCode').val();
		var data11 = $('#timeNHDate').val();
		var data12 = $('#timeNHLocation').val();
		var data13 = $('#timeNotesPage textarea').val();
		var data14 = setEditDate();
		transaction.executeSql("INSERT INTO timesheets (mattId, timeDate, courtPolice, rate, duration, activity, dscc, comment, hearingType, "+
			"repOrder, maatID, nextHearing, location, notes, editDate)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, data11, data12, data13, data14]);
		console.log("Insert Time Query Succeded");
	});
}

// Update time record
function updateTimeRecord(id) {
	TIMEAPPDB.transaction(function(transaction) {
		var data0 = $('#selectMatterAT').data('mattID');
		var data1 = $('#timeDate').val();
		var data2 = $('#courtPoliceID').val();
		var data3 = $('#timeRate').val();
		var data4 = $('#timeDurationQty').val();
		var data5 = $('#timeActivity').val();
		var data6 = $('#timeDSCC').val();		
		var data7 = $('#timeComment').val();
		var data8 = $('#timeHearingType').val();
		var data9 = $('#timeRepOrderDate').val();
		var data10 = $('#timeMaatCode').val();
		var data11 = $('#timeNHDate').val();
		var data12 = $('#timeNHLocation').val();
		var data13 = $('#timeNotesPage textarea').val();
		var data14 = setEditDate();
		transaction.executeSql("UPDATE timesheets SET mattId ='"+data0+"', timeDate ='"+data1+"', courtPolice ='"+data2+"', rate ='"+data3+"', duration ='"+data4+"', activity ='"+data5+"',"+
			"dscc ='"+data6+"', comment ='"+data7+"', hearingType ='"+data8+"', repOrder ='"+data9+"', maatID ='"+data10+"', nextHearing ='"+data11+"', location ='"+data12+"', notes ='"+data13+"',"+
			"editDate ='"+data14+"' WHERE timeId = "+id+";");
		console.log("Update Time Query Succeded");
	});
}

// Delete time record 
function deleteTimeRecord(id){
	TIMEAPPDB.transaction(function(transaction){
		transaction.executeSql("DELETE FROM timesheets WHERE timeId = "+id+";");
	});
	console.log("Delete Time Query Succeded");
}

// Generate timesheets list
function loadTimeList(date){	
	$('#timePage ul li:gt(0)').remove();
	var totalTime = 0;
	TIMEAPPDB.transaction(function(transaction){
		transaction.executeSql("SELECT timesheets.timeId, timesheets.duration, timesheets.comment, timesheets.activity, timesheets.timeDate, matters.mattNo, matters.clientForename,"+
			"matters.clientSurname, matters.matterType FROM timesheets INNER JOIN matters ON timesheets.mattId = matters.mattId WHERE timesheets.timeDate = '"+date+"';", undefined, function(transaction, result){
			for(i = 0; i < result.rows.length; i++){
				//$('#timeListEmpty').css("display", "none");
				//$('#timeListEmpty').empty();
				var row = result.rows.item(i);
				var newEntryRow = $('#timeTemplate').clone();				
				newEntryRow.removeAttr('style');
				newEntryRow.data('entryId', row.id);
				newEntryRow.appendTo('#timePage ul');
				newEntryRow.data("timeID", row.timeId);
				newEntryRow.find('span:first').text(row.mattNo+" "+row.clientSurname+", "+row.clientForename.charAt(0));
				newEntryRow.find('span:nth-child(2)').text(row.duration+" min");
				newEntryRow.find('span:nth-child(4)').text(row.comment);
				newEntryRow.find('span:last').text(row.activity);
				totalTime += parseFloat(row.duration);
			}
			$('#totalTime').val(calculateTime(totalTime));
		}, errorHandler);	
	});
}

// Generate matters list for popup
function loadMatterPopupList(){
	$('#selectMatterPopup ul li:gt(0)').remove();
	TIMEAPPDB.transaction(function(transaction){
		transaction.executeSql('SELECT mattId, mattNo, clientForename, clientSurname, matterType FROM matters ORDER BY clientSurname;', undefined, function(transaction, result){
			for(i = 0; i < result.rows.length; i++){
				var row = result.rows.item(i);
				var newEntryRow = $('#matterPopupTemplate').clone();				
				newEntryRow.removeAttr('style');
				newEntryRow.data('entryId', row.id);
				newEntryRow.appendTo('#selectMatterPopup ul');
				newEntryRow.data("mattID", row.mattId);
				newEntryRow.find('span:first').text(row.mattNo);
				newEntryRow.find('span:nth-child(2)').text(row.clientForename+", "+row.clientSurname);
				newEntryRow.find('span:last').text(row.matterType);
			}
		}, errorHandler);	
	});
}

// Set time details - view time screen
function viewTimeDetails(id){
	TIMEAPPDB.transaction (function(transaction){
		transaction.executeSql("SELECT timesheets.timeId, timesheets.mattId, timesheets.timeDate, timesheets.courtPolice, timesheets.rate, timesheets.duration, timesheets.activity, timesheets.dscc, timesheets.comment, "+
			"timesheets.hearingType, timesheets.repOrder, timesheets.maatID, timesheets.nextHearing, timesheets.location, timesheets.notes, matters.mattNo, matters.clientForename, matters.clientSurname, matters.matterType  "+
			"FROM timesheets INNER JOIN matters ON timesheets.mattId = matters.mattId WHERE timeId = "+id+";", undefined, function(transaction, result){
			for(i = 0; i < result.rows.length; i++){
				var row = result.rows.item(i);
				$('#selectMatterAT').data('mattID', row.mattId);
    			$('#selectMatterAT span:first').text(row.mattNo);
    			$('#selectMatterAT span:nth-child(2)').text(row.clientForename+", "+row.clientSurname);
    			$('#selectMatterAT span:last').text(row.matterType);
				$('#timeDate').val(row.timeDate);
				$('#courtPoliceID').val(row.courtPolice);
				$('#timeRate').val(row.rate);
				$('#timeDurationQty').val(row.duration);
				$('#timeActivity').val(row.activity);
				$('#timeDSCC').val(row.dscc);		
				$('#timeComment').val(row.comment);
				if(row.repOrder !== ""){
					$('.timeCourtSelection').show(); 
					$('#timeHearingType').val(row.hearingType);
					$('#timeRepOrderDate').val(row.repOrder);
					$('#timeMaatCode').val(row.maatID);
					$('#timeNHDate').val(row.nextHearing);
					$('#timeNHLocation').val(row.location);
				}else{
					$('.timeCourtSelection').hide(); 
				}
				$('#timeNotesPage textarea').val(row.notes);
				$('#saveTimeButt').data('timeID', id)
			}
		}, errorHandler);	
	});	
}

// Calculate hours & minutes based on minutes
function calculateTime(min){
	var hours = Math.floor(min / 60),        
    	minutes = min % 60;
    return hours === 0 ? minutes+" mins" : hours+" Hr "+minutes+" mins";
}