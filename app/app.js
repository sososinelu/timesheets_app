//*********************************
// D O M - F U N C T I O N S
//*********************************

// Suppress all page transitions
$(document).bind('pageinit', function (){
    $.mobile.defaultPageTransition = 'none';
});

$(function() {
// Initialise the database
initDatabase();

/* ------------------ LOGIN PAGE CLICK FUNCTIONS ------------------ */

// Login button
$("#loginPage").on('tap', 'a', function(){
    $('#mainPage h3').text($('#loginPage select').val());
});

/* ------------------ MAIN PAGE CLICK FUNCTIONS ------------------ */

// Timesheets
$("#mainPage").on("tap", "#loadTimePage", function(){
    var date = new Date();
    $('#filterTimeByDate').val(date.dateNow());
    loadTimeList(date.dateNow());
});
// Matters
$("#mainPage").on("tap", "#loadMatterPage", loadMattersList);
// Sync Data
$('#mainPage').on('tap', '#loadSyncPage', function(){
    
});

/* ------------------ TIMESHEETS CLICK FUNCTIONS ------------------ */

// Time list select date
$('#filterTimeByDate').change(function(){
    loadTimeList($(this).val());
});
// Time List tap
$("#timePage").on("tap", "ul li:gt(0)", function(){
    $('#addTimePage h4').text('Time');
    viewTimeDetails($(this).data('timeID'));
});
// Long tap on the time list
$("#timePage").on("taphold", "ul li:gt(0)", function(){
    // Set delete time popup details + data
    $('#deleteTimePopup').find('span:first').text($(this).find('span:first').text());
    $('#deleteTimePopup').find('span:nth-child(2)').text($(this).find('span:nth-child(2)').text());
    $('#deleteTimePopup').find('span:nth-child(4)').text($(this).find('span:nth-child(4)').text());
    $('#deleteTimePopup').find('span:last').text($(this).find('span:last').text());
    $('#deleteTimePopup').find('#deleteTimeButt').data('timeID', $(this).data('timeID'));
    $('#deleteTimePopup').popup('open');
});
// Delete time
$('#timePage').on('tap', '#deleteTimeButt', function(){
    deleteTimeRecord($(this).data('timeID'));
    loadTimeList($('#filterTimeByDate').val());
}); 
// Time List - add time button
$('#timePage').on('tap', '#addTimePageButt', function(){
    $('#addTimePage h4').text('Add Time');
    $('#addTimeForm')[0].reset(); // Reset add time form
    $('#timeNotesPage textarea').val(""); // Reset notes textarea
    $('#timeDate').val($('#filterTimeByDate').val());
    $('#timeRepOrderDate').val($('#filterTimeByDate').val());
    

});
// Select matter popup - 
$('#addTimePage').on('tap', '#selectMatterAT', loadMatterPopupList);
// Matter popup list - set matter details on add time page
$('#selectMatterPopup').on('tap', 'ul li:gt(0)', function(){
    $('#selectMatterAT').data('mattID', $(this).data('mattID'));
    $('#selectMatterAT span:first').text($(this).find('span:first').text());
    $('#selectMatterAT span:nth-child(2)').text($(this).find('span:nth-child(2)').text());
    $('#selectMatterAT span:last').text($(this).find('span:last').text());
});
// Display court options based on time rate
$('#timeRate').change(function() {
    if($('#timeRate').val() === "2C" || $('#timeRate').val() === "2H" || $('#timeRate').val() === "2M"){
        $('.timeCourtSelection').show(); 
    }else{
        $('.timeCourtSelection').hide();
    }
});
// Save time button
$('#addTimePage').on('tap', '#saveTimeButt', function(){
    if($('#addTimePage h4').text() === 'Add Time'){
        insertTimeRecord();
    }else{
        updateTimeRecord($(this).data('timeID'));
    }
    loadTimeList($('#filterTimeByDate').val());
});

/* ------------------ MATTERS CLICK FUNCTIONS ------------------ */

// Matters List tap
$("#mattersPage").on("tap", "ul li:gt(0)", function(){
    $('#addMatterPage h4').text('Matter');
    //$('#saveMatterButt').attr('value', 'Save Matter Changes');
    //$('#saveMatterButt').button('refresh');
    viewMatterDetails($(this).data('mattID'));
});
// Add Matter button
$("#mattersPage").on("tap", "#addMatterPageButt", function(){
    $('#addMatterPage h4').text('Add Matter');
    //$('#saveMatterButt').attr('value', 'Save Matter');
    //$('#saveMatterButt').button('refresh');
    $('#addMatterForm')[0].reset(); // Reset add matter form
    var date = new Date();
    $('#mattDateField').val(date.dateNow());
});
// Save matter button
$("#addMatterPage").on("tap", "#saveMatterButt", function(){ 
    if($('#addMatterPage h4').text() === 'Add Matter'){
        insertMatterRecord(); 
    }else{
        updateMatterRecord($(this).data('mattID'));
    }
    loadMattersList(); 
});

/* ------------------ SETTINGS CLICK FUNCTIONS ------------------ */

$('#settingsPage').on('tap', '#loadTestDataButt', function(){
    dropTables();
    initDatabase();
    loadTestData();
    $('#testDataLoadedPopup').popup('open');
});


});