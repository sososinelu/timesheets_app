//****************************
// M A T T E R S
//****************************

// Insert matter record
function insertMatterRecord(){
	TIMEAPPDB.transaction( function(transaction) {
		var data0 = $('#mattNoField').val();
		var data1 = $('#mattDateField').val();
		var data2 = $('#mattUFNField').val();
		var data3 = $('#mattWorkTypeSelect').val();
		var data4 = $('#mattCliForenameField').val();
		var data5 = $('#mattCliSurnameField').val();
		var data6 = $('#mattCliDobField').val();
		var data7 = $('#mattTypeSelect').val();
		var data8 = $('#mattChargesField').val();
		var data9 = setEditDate();
		transaction.executeSql ("INSERT INTO matters (mattNo, opened, ufn, workType, clientForename, clientSurname, clientDob, matterType, charges, "+
			"editDate)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [data0, data1, data2, data3, data4, data5, data6, data7, data8, data9]);
		console.log("Insert Matter Query Succeded");
	});
}

// Update matter record
function updateMatterRecord (id) {
	TIMEAPPDB.transaction (function(transaction) {
		transaction.executeSql ("UPDATE matters SET mattNo = '"+$('#mattNoField').val()+"', opened = '"+$('#mattDateField').val()+"', ufn = '"+$('#mattUFNField').val()+"', "+
			"workType = '"+$('#mattWorkTypeSelect').val()+"', clientForename = '"+$('#mattCliForenameField').val()+"', clientSurname = '"+$('#mattCliSurnameField').val()+"', "+
			"clientDob = '"+$('#mattCliDobField').val()+"', matterType = '"+$('#mattTypeSelect').val()+"', charges = '"+$('#mattChargesField').val()+"', editDate = '"+setEditDate()+"' "+
			"WHERE mattId = "+id+";");
		console.log("Update Matter Query Succeded");
	});
}

// Generate matters list
function loadMattersList(){	
	$('#mattersPage ul li:gt(0)').remove();
	TIMEAPPDB.transaction(function(transaction){
		transaction.executeSql('SELECT mattId, mattNo, clientForename, clientSurname, matterType FROM matters ORDER BY clientSurname;', undefined, function(transaction, result){
			for(i = 0; i < result.rows.length; i++){
				var row = result.rows.item(i);
				var newEntryRow = $('#matterTemplate').clone();				
				newEntryRow.removeAttr('style');
				newEntryRow.data('entryId', row.id);
				newEntryRow.appendTo('#mattersPage ul');
				newEntryRow.data("mattID", row.mattId);
				newEntryRow.find('span:first').text(row.mattNo);
				newEntryRow.find('span:nth-child(2)').text(row.clientForename+", "+row.clientSurname);
				newEntryRow.find('span:last').text(row.matterType);
			}
		}, errorHandler);	
	});
}

// Set matters details - view matter screen
function viewMatterDetails(id){
	TIMEAPPDB.transaction (function(transaction){
		transaction.executeSql('SELECT mattNo, opened, ufn, workType, clientForename, clientSurname, clientDob, matterType, charges FROM matters WHERE mattId = '+id+';', undefined, function(transaction, result){
			for(i = 0; i < result.rows.length; i++){
				var row = result.rows.item(i);
				$('#mattNoField').val(row.mattNo);
				$('#mattDateField').val(row.opened);
				$('#mattUFNField').val(row.ufn);	
				$('#mattWorkTypeSelect').val(row.workType);
				$('#mattWorkTypeSelect').selectmenu('refresh');
				$('#mattCliForenameField').val(row.clientForename);
				$('#mattCliSurnameField').val(row.clientSurname);
				$('#mattCliDobField').val(row.clientDob);
				$('#mattTypeSelect').val(row.matterType);
				$('#mattTypeSelect').selectmenu('refresh');
				$('#mattChargesField').val(row.charges);
				$('#saveMatterButt').data('mattID', id);
			}
		}, errorHandler);	
	});	
}